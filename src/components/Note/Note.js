import React, { Component } from 'react'
import './Note.css'
import PropTypes from 'prop-types'

class Note extends Component {
    constructor(props) {
        super(props)

        this.noteContent = props.noteContent
        this.noteId = props.noteId

        this.handleRemoveNote = this.handleRemoveNote.bind(this)
    }

    handleRemoveNote(id) {
        this.props.removeNote(id)
    }

    render(props) {

        const { noteContent } = this.props

        return (
            <div className="note fade-in">
                <div>
                    <button
                        className="closebtn"
                        onClick={() => this.handleRemoveNote(this.noteId)}>
                        Done
                    </button> 
                </div>
                <p className="noteContent">{ noteContent }</p>
            </div>
        )
    }
}

Note.PropTypes = {
  noteContent: PropTypes.string
}

export default Note