import React, { Component } from 'react'
import './NoteForm.css'

class NoteForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            newNoteContent: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    handleChange(e) {
        this.setState({
            newNoteContent: e.target.value
        })

        // console.log(this.state.newNoteContent)
    }

    handleClick() {
        const { newNoteContent } = this.state


        // set the noteContent for a note to the value of the input
        this.props.addNote(newNoteContent)

        // set newNoteContent back to empty string
        this.setState({
            newNoteContent: ''
        })
    }

    render(props) {

        const { newNoteContent } = this.state

        return (
            <div className="formWrapper">
                <input
                    className="noteInput"
                    type="text"
                    placeholder="Write a new note"
                    value={newNoteContent}
                    onChange = {this.handleChange}/>
                <button className="noteButton" onClick={this.handleClick}>Add a note</button>
            </div>
        )
    }
}

export default NoteForm