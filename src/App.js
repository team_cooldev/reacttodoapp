import React, { Component } from 'react';
import './App.css';
import Note from './components/Note/Note'
import NoteForm from './components/NoteForm/NoteForm'
import { DB_CONFIG } from './config'
import 'firebase/database'
import firebase from 'firebase/app'

class App extends Component {

  constructor(props) {
    super(props)

    this.app = firebase.initializeApp(DB_CONFIG)
    this.db = this.app.database().ref().child('todos')

    this.state = {
      notes: []
    }

    this.addNote = this.addNote.bind(this)
    this.removeNote = this.removeNote.bind(this)
  }

  componentWillMount() {
    const notesArray = this.state.notes

    this.db.on('child_added', snapshot => {
      notesArray.push({
        id: snapshot.key,
        noteContent: snapshot.val().todoContent,
      })
    })

    this.setState({
      notes: notesArray
    })

    this.db.on('child_removed', snapshot => {
      for (let i = 0; i < notesArray.length; i++) {
        if(notesArray[i] === snapshot.key) {
          notesArray.splice(i,1)
        }
      }
    })

    this.setState({
      notes: notesArray
    })
  }

  componentDidMount() {
    this.db.on('value', snapshot => {
      let dbData = snapshot.val()
      let dbNotes = []

      for (let data in dbData) {
        // console.log(datas[data].todoContent)
        dbNotes.push({
          id: data,
          noteContent: dbData[data].todoContent
        })
      }

      this.setState({
        notes: dbNotes
      })
    })
  }

  addNote(note) {
    this.db.push().set({ todoContent: note })
  }

  removeNote(noteId) {
    this.db.child(noteId).remove()
  }

  render() {
    return (
      <div className="notesWrapper">
        <div className="notesHeader">
          <div className="heading">
            <h1>React & Firebase Todo list</h1>
          </div>
          </div>
          <div className="notesBody">
            { this.state.notes.map((note)=> {
                return( 
                    <Note
                      noteContent={note.noteContent}
                      noteId={note.id}
                      key={note.id}
                      removeNote={this.removeNote}/>                
              )})}
          </div>
        <div className="notesFooter">
            <NoteForm  addNote={this.addNote}/>
        </div>
      </div>
    );
  }
}

export default App;
